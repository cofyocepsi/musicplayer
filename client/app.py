import time
import youtube_dl
import ffmpeg
import shutil
from pydub import AudioSegment
from pydub.playback import play
import glob
import sys
import os
import argparse
import urllib
import json
import subprocess
from multiprocessing import Process, Queue, Lock
import pyaudio
import multiprocessing
import wave

parser = argparse.ArgumentParser(description='Interact with a musicplayer API endpoint')
parser.add_argument('-r', '--remote', help="Set remote server domain", required=True)
parser.add_argument('-v', '--verbose', action="store_true", help="Enable verbose logging")
parser.add_argument('-n', '--noclear', action="store_true", help="Do not clear screens inbetween loops")
parser.add_argument('-i', '--insecure', action="store_true", help="Use HTTP instead of HTTPS")
parser.add_argument('-s', '--single-loop', action="store_true", help="Do not loop")
args = parser.parse_args();

print('Copyright © 2020 cofyocepsi@protonmail.com')
print('This project is licensed under the AGPL and its source can be found at https://gitlab.com/cofyocepsi/musicplayer')
time.sleep(2)

try:
	with open('../config/apikey', 'r') as keyfile:
		apikey = keyfile.read()
except:
	print('You need to set your api key at config/apikey')
	exit(1)

# Set the URL protocol and domain
url = 'http' if args.insecure else 'https' # Select protocol
url += '://%s' % args.remote # Set domain

ydl_opts = {'quiet': True, 'no_warnings': True, 'format': 'bestaudio', 'outtmpl': 'song', 'noplaylist': True, 'playlistend': 1}

while True:
	# Clear screen
	if not args.noclear:
		os.system('cls' if os.name == 'nt' else 'clear')

	# Request the song that should be playing
	apirequest = urllib.request.Request('%s/api/?action=nowPlaying' % url)
	songrequest = json.loads(urllib.request.urlopen(apirequest).read())

	if not songrequest:
		print('No songs in queue, checking again in five seconds')
		time.sleep(5)
	else:
		# Download song
		with youtube_dl.YoutubeDL(ydl_opts) as ydl:
			ydl.download([songrequest['songlink']])

		#  Convert to .wav
		ffmpegargs = (ffmpeg
			.input(glob.glob('song')[0])
			.output('song.wav')
			.overwrite_output()
			.get_args()
		)
		conversion = subprocess.Popen(['ffmpeg'] + ffmpegargs, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
		conversion.wait() # Wait for the conversion to finish

		# Play song, then remove the temporary files
		print('Now playing %s (%s) as requested by %s. Press space to pause/unpause' % (songrequest['songname'], songrequest['songlink'], songrequest['requester']))
		song = AudioSegment.from_wav('song.wav')
		def setup(event):
			global unpaused
			unpaused = event

		def playSong(event):
			chunk = 1024

			# open the file for reading.
			wf = wave.open('song.wav', 'rb')

			# create an audio object
			p = pyaudio.PyAudio()

			# open stream based on the wave object which has been input.
			stream = p.open(format =
				p.get_format_from_width(wf.getsampwidth()),
				channels = wf.getnchannels(),
				rate = wf.getframerate(),
				output = True)

			# read data (based on the chunk size)
			data = wf.readframes(chunk)

			# play stream (looping from beginning of file to the end)
			while data != bytes('', 'ascii'):
				unpaused.wait()
				# writing to the stream is what *actually* plays the sound.
				stream.write(data)
				data = wf.readframes(chunk)

			# cleanup stuff.
			stream.close()
			p.terminate()

		event = multiprocessing.Event()
		playProcess = Process(target=playSong, args=(setup(event), ))
		playProcess.start()
		event.set()
		while(playProcess.is_alive()):
			import os

			# Windows
			if os.name == 'nt':
				import msvcrt

			# Posix (Linux, OS X)
			else:
				import sys
				import termios
				import atexit
				from select import select


			class KBHit:

				def __init__(self):
					'''Creates a KBHit object that you can call to do various keyboard things.
					'''

					if os.name == 'nt':
						pass

					else:

						# Save the terminal settings
						self.fd = sys.stdin.fileno()
						self.new_term = termios.tcgetattr(self.fd)
						self.old_term = termios.tcgetattr(self.fd)

						# New terminal setting unbuffered
						self.new_term[3] = (self.new_term[3] & ~termios.ICANON & ~termios.ECHO)
						termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.new_term)

						# Support normal-terminal reset at exit
						atexit.register(self.set_normal_term)


				def set_normal_term(self):
					''' Resets to normal terminal.  On Windows this is a no-op.
					'''

					if os.name == 'nt':
						pass

					else:
						termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.old_term)


				def getch(self):
					''' Returns a keyboard character after kbhit() has been called.
						Should not be called in the same program as getarrow().
					'''

					s = ''

					if os.name == 'nt':
						return msvcrt.getch().decode('utf-8')

					else:
						return sys.stdin.read(1)


				def getarrow(self):
					''' Returns an arrow-key code after kbhit() has been called. Codes are
					0 : up
					1 : right
					2 : down
					3 : left
					Should not be called in the same program as getch().
					'''

					if os.name == 'nt':
						msvcrt.getch() # skip 0xE0
						c = msvcrt.getch()
						vals = [72, 77, 80, 75]

					else:
						c = sys.stdin.read(3)[2]
						vals = [65, 67, 66, 68]

					return vals.index(ord(c.decode('utf-8')))


				def kbhit(self):
					''' Returns True if keyboard character was hit, False otherwise.
					'''
					if os.name == 'nt':
						return msvcrt.kbhit()

					else:
						dr,dw,de = select([sys.stdin], [], [], 0)
						return dr != []


			kb = KBHit()

			while True:
				for x in range(1, 100):
					if kb.kbhit():
						c = kb.getch()
						if ord(c) == 32: # Space
							event.clear()
							print('Paused')
							while True:
								if kb.kbhit():
									c = kb.getch()
									if ord(c) == 32: # Space
										event.set()
										print('Unpaused')
										break
					time.sleep(0.02) # Check for skip every two seconds

				# Check for skip
				request = urllib.request.Request('%s/api/?action=trackExists&id=%s&apikey=%s' % (url, songrequest['id'], apikey))
				shouldDelete = json.loads(urllib.request.urlopen(request).read())

				# Is song skipped?
				if shouldDelete == 1:
					# Stop playback
					playProcess.terminate()
					playProcess.join()
					kb.set_normal_term()

					# Only remove it locally since it is already removed on the server
					os.remove('song')
					os.remove('song.wav')
					break

				# Has playback finished?
				if not playProcess.is_alive():
					kb.set_normal_term()

					# Remove the song both locally and remotely
					os.remove('song')
					os.remove('song.wav')

					request = urllib.request.Request('%s/api/?action=delete&rank=1&apikey=%s' % (url, apikey))
					try:
						urllib.request.urlopen(request)
					except:
						# No song exists
						pass
					break
