// Copyright © 2020 cofyocepsi@protonmail.com
//
// This file is part of https://gitlab.com/cofyocepsi/musicplayer.
//
// This project is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This project is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this project. If not, see <https://www.gnu.org/licenses/>.# Copyright © 2020 cofyocepsi@protonmail.com

const http = require('http');

const hostname = '0.0.0.0';
const port = 12345;

const server = http.createServer((req, res) => {
	res.statusCode = 200;
	res.setHeader('Content-Type', 'text/plain');
});

fs = require('fs')
fs.readFile('config/oauth', 'utf8', function (err,data) {
	if (err) {
		return console.log(err);
		process.exit()
	}
	oauth = data

	server.listen(port, hostname, () => {
		const tmi = require('tmi.js');

		// Define configuration options
		fs.readFile('config/channelname', 'utf8', function (err, data) {
			const botname = data
			fs.readFile('config/botname', 'utf8', function (err, channelname) {
				const opts = {
					identity: {
						username: botname,
						password: oauth
					},
					channels: [
						channelname
					]
				};

				// Create a client with our options
				const client = new tmi.client(opts);

				// Register our event handlers (defined below)
				client.on('message', onMessageHandler);
				client.on('connected', onConnectedHandler);

				// Connect to Twitch:
				client.connect();

				// Called every time a message comes in
				function onMessageHandler (target, context, msg, self) {
					if (self) { return; } // Ignore messages from the bot

					// Remove whitespace from chat message
					const commandName = msg.trim();

					args = msg.split(' ');
					if(args[0] === '!sr') {
						requestSong(args, target, context)
					} else if(args[0] === '!srskip'){
						skipSong(target, context);
					} else if(args[0] === '!srnow') {
						listSongs(target, context);
					}

					console.log(`* Executed ${context['username']}'s command: ${commandName}`)
				}

				function listSongs(target, context) {
					var needle = require('needle');
					needle('get', 'http://172.17.0.1:80/api/?action=nowPlaying')
					.then((res) => {
						if(res.statusCode === 200) {
							parsed = JSON.parse(res.body);
							if(parsed) {
								client.say(target, `@${context['username']}, the current song is ${parsed['songname']} (${parsed['songlink']})`);
							} else {
								client.say(target, `@${context['username']}, there are currently no songs in the queue`);
							}
						}
					}).catch((err) => {
						console.error(err);
					});
				}

				function skipSong(target, context) {
					requester = context['username'];
					// Must be mod or broadcaster
					if(context['mod'] || context['badges-raw'].includes('broadcaster/1')) {
						// Read API key
						apikey = fs.readFile('config/apikey', 'utf8', function (err,apikey) {
						if (err) {
							return console.log(err);
							process.exit();
						}

						// Send the add request to the API
						var needle = require('needle');
						needle('get', `http://172.17.0.1:80/api/?action=delete&apikey=${apikey}&rank=1`)
							.then((res) => {
								if(res.statusCode === 200) {
									client.say(target, `Skipped song ${res.body}`)
								} else {
									client.say(target, "Couldn't skip song because the queue is empty")
								}
							}).catch((err) => {
								console.error(err);
							});
						});
					} else {
						client.say(target, `Only mods can skip songs, @${requester}`)
					}
				}

				function requestSong(args, target, context) {
					// Must be sub, mod or broadcaster
					if(context['subscriber'] || context['mod'] || context['badges-raw'].includes('broadcaster/1')) {
						// Read API key
						apikey = fs.readFile('config/apikey', 'utf8', function (err,apikey) {
							if (err) {
								return console.log(err);
								process.exit();
							}

						// Send the add request to the API
						var needle = require('needle');
						requester = context['username'];
						needle('get', `http://172.17.0.1:80/api/?action=add&apikey=${apikey}&songlink=`+encodeURIComponent(args[1])+`&requester=${requester}`)
							.then((res) => {
								switch(res.statusCode) {
									// Something was wrong with the request
									case 400:
										switch(res.body) {
											case 'invalidFormatting':
												toSay = `Sorry @${requester}, your song was not added due to the link being incorrectly formatted`;
												break;
											case 'invalidURL':
												toSay = `Sorry @${requester}, the song you requested either doesn't exist or is geoblocked`;
												break;
										}
										break;
									// The request went through
									case 200:
										toSay = `@${requester}, your song request for ${args[1]} was added at position ${res.body}`;
										break;
								}
								// Send the response
								client.say(target, toSay);
							}).catch((err) => {
								console.error(err);
							});
						});
					}
				}
				// Called every time the bot connects to Twitch chat
				function onConnectedHandler (addr, port) {
					console.log(`* Connected to ${addr}:${port}`);
				}
			});
					})
				})
});
