# Music player
A program meant for (primarily Twitch) streamers who want viewer-selected music. There are three parts to it:
1. The Twitch bot (Node.js) - For basic actions (requesting songs, skipping songs and showing the current song)
2. A local script for the streamer (Python) - Plays the first song in the queue. It has the ability to pause the song
3. The web client (LEMP) - For more advanced actions (such as seeing the entire queue and who requested what and deleting and reordering songs). These actions are authenticated with one optional token for the users (abuse potential since users can lie about their identity/impersonate those requesting through Twitch) and one token for the mods, both of which can be regenerated in case of a leak. The user token is only meant for usage outside of Twitch, as the Twitch API provides much safer authentication. The server does not have to be public, but it is recommended to give at least yourself/a mod access to it as your control will otherwise be severely limited. If you want your viewers to see the queue, this has to be public. Warning - Unless only hosted locally (e.g. no one else has access), this will reveal the server's IP and, if you're hosting it at home, your home IP.

## Available Twitch commands
	```
	!sr link - Subscribers, requests song
	!srskip - Mods/stremer, skips current song
	!srnow - All users, displays current song
	```

## How to set up the local script (on the computer you are streaming from)
1. Clone the repository and the client directory
	```
	git clone https://gitlab.com/cofyocepsi/musicplayer
	cd musicplayer/client
	```

2. Install the dependencies
	```
	pip install --user -r requirements.txt
	```

3. Run the client (set up the server first)
	```
	# Run python3.7 app.py in addition replacing pip with pip3.7 above if this doesn't work
	# Replace REMOTE with the remote domain, so if the web server is at https://example.com, set it to example.com
	# If the connection isn't using HTTPS, add the -i flag (only use in testing)
	# See python app.py -h for all available options
	python app.py -r REMOTE
	```

4. If running the server part remotely, set the API key locally as well
	```
	# Must be the same as on the server
	# You can get the key from the server through "cat config/apikey"
	# DO NOT SHARE THIS WITH ANYONE WHEN USED IN PRODUCTION
	echo -n "YOURAPIKEYHERE" > config/apikey
	```



## How to set up the server (optionally on a remote computer)
1. Clone the repository and enter it (unless you already did so while setting up the local script)
	```
	git clone https://gitlab.com/cofyocepsi/musicplayer
	cd musicplayer
	```

2. Set config files
	```
	timezone='Region/City' # E.g. Europe/Stockholm
	mkdir config

	# This is the secret used to authenticate the streamer's script and the Node.js server with the webserver
	# You do not need to memorize this, so make it rather long and complicated (but only a-zA-Z0-9 to stay safely within the GET parameter's limitations)
	# DO NOT SHARE THIS WITH ANYONE WHEN USED IN PRODUCTION
	echo -n "YOURAPIKEYHERE" > config/apikey

	# This is the token used to authenticate the Node.js server with Twitch's IRC channels
	# Replace the example token below with the one retrieved from here: https://twitchapps.com/tmi/
	# DO ABSOLUTELY NOT SHARE THIS WITH ANYONE, NOT EVEN OUTSIDE OF PRODUCTION
	echo "oauth:abcdefghijklmnopqrstuvwxyz0123" > config/oauth

	# Set your bot name and channel name. They can be the same, but don't have to
	# If the bot isn't the streamer's account, keep in mind that the token above has to be for the bot
	echo -n "YOURBOTNAME" > config/botname
	echo -n "YOURCHANNELNAME" > config/channelname
	```

3. Set the timezone for PHP:
	```
	echo $timezone > config/timezone
	```

4. Install the bot npm modules
	```
	cd bot
	npm install
	cd ..
	```

5. Start the containers
	```
	docker-compose up -d
	```

6. Set the timezone for MySQL:
Note: You have to wait for the MySQL container to initialize before running the command
	```
	docker exec mysql bash -c "mysql_tzinfo_to_sql /usr/share/zoneinfo/"$timezone" "$timezone" | mysql -ppassword mysql"
	```

7. Add and authenticate with a mod auth token - See below

## Manage mod auth token
## Authenticate with the token
Go to http://localhost/auth/?token=YOURTOKENHERE. This will overwrite any previous session (such as a user one)

### Set the token
	```
	modToken="SETYOURTOKENHERE"

	docker exec mysql mysql -ppassword -e "INSERT INTO database.sessions (uuid, authtype) VALUES ("$modToken", 1);"
	```

### See the token
	```
	docker exec mysql mysql -ppassword -e "SELECT uuid FROM database.sessions WHERE authtype=1;"
	```

### Reset the token
	```
	modToken="SETYOURTOKENHERE"

	docker exec mysql mysql -ppassword -e "DELETE FROM database.sessions WHERE authtype=1;"
	docker exec mysql mysql -ppassword -e "INSERT INTO database.sessions (uuid, authtype) VALUES ('"$modToken"', 1);"
	```

## Update
	```

	git pull
	docker-compose down

	# The following five commands are only necessary when mysql/setup.sql has changed
	# Warning - This will clear all data
	timezone='Region/City' # E.g. Europe/Stockholm
	modToken="SETYOURTOKENHERE"

	rm -rf db-data
	docker exec mysql bash -c "mysql_tzinfo_to_sql /usr/share/zoneinfo/"$timezone" "$timezone" | mysql -ppassword mysql"
	docker exec mysql mysql -ppassword -e "INSERT INTO database.sessions (uuid, authtype) VALUES ("$modToken", 1);"

	docker-compose up -d
	```
Now re-set your mod auth token and you're good to go
