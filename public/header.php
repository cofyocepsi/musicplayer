<?php
	# Copyright © 2020 cofyocepsi@protonmail.com
	#
	# This file is part of https://gitlab.com/cofyocepsi/musicplayer.
	#
	# This project is free software: you can redistribute it and/or modify
	# it under the terms of the GNU Affero General Public License as published by
	# the Free Software Foundation, either version 3 of the License, or
	# (at your option) any later version.
	#
	# This project is distributed in the hope that it will be useful,
	# but WITHOUT ANY WARRANTY; without even the implied warranty of
	# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	# GNU Affero General Public License for more details.
	#
	# You should have received a copy of the GNU Affero General Public License
	# along with this project. If not, see <https://www.gnu.org/licenses/>.

	# Start session
	session_start();

	# Create DB connection
	$dbConn = new PDO("mysql:host=172.17.0.1;dbname=database;charset=utf8mb4", 'root', 'password', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
	# Set error mode
	$dbConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	# Disable preparation emulation
	$dbConn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

	# This will execute $query with $values bound to $vars and if $returnResult is true it will return the first result. Use the original $query to access the generated PDO object
	function sqlQuery(&$query, $vars = [], $values = [], $returnResult = false) {
		# Access the connection to the database
		$dbConn = $GLOBALS['dbConn'];

		# Prepare the query and bind all parameters
		$query = $dbConn->prepare($query);
		for($i = 0; $i < count($values); $i++) {
			$query->bindParam($vars[$i], $values[$i]);
		}

		$query->execute();

		# This can be used if the query only returns one result instead of calling sqlGetResult() manually
		if($returnResult) {
			return sqlGetResult($query);
		}
	}

	# Get one result from a query
	function sqlGetResult($query) {
		return $query->fetch(PDO::FETCH_ASSOC);
	}

	# Insert all query results into an array
	function sqlResultsToArray($query, &$array) {
		while($array[] = sqlGetResult($query)) {}

		# Remove the trailing bool(false)
		array_pop($array);
	}

	# Set timezone
	$timezone = fopen($_SERVER['DOCUMENT_ROOT'].'/config/timezone', 'r');
	$timezone = fread($timezone, filesize($_SERVER['DOCUMENT_ROOT'].'/config/timezone')-1);
	date_default_timezone_set($timezone);

	# Set timezone for MySQL
	$query = "SET time_zone = '$timezone'";
	sqlQuery($query);

	# Ensure that the token exists
	if(empty($_SESSION['token'])) {
		# Is empty, give the session guest status
		$_SESSION['token'] = [0, -1];
	}

	# Determine if the token is set and valid
	$query = 'SELECT COUNT(1) FROM sessions WHERE uuid=:uuid';
	$vars = ['uuid'];
	$values = [$_SESSION['token'][0]];

	if(!sqlQuery($query, $vars, $values, true)['COUNT(1)']) {
		# Log out if the token isn't valid
		$_SESSION['token'] = [0, -1];
	}
