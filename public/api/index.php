<?php
	# Copyright © 2020 cofyocepsi@protonmail.com
	#
	# This file is part of https://gitlab.com/cofyocepsi/musicplayer.
	#
	# This project is free software: you can redistribute it and/or modify
	# it under the terms of the GNU Affero General Public License as published by
	# the Free Software Foundation, either version 3 of the License, or
	# (at your option) any later version.
	#
	# This project is distributed in the hope that it will be useful,
	# but WITHOUT ANY WARRANTY; without even the implied warranty of
	# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	# GNU Affero General Public License for more details.
	#
	# You should have received a copy of the GNU Affero General Public License
	# along with this project. If not, see <https://www.gnu.org/licenses/>.

	require($_SERVER['DOCUMENT_ROOT'].'/header.php');

	if(empty($_GET['action'])) {
		die();
	}

	function actionIsAllowed($permissionLevel, $requiredParams = []) {
		# Select the right auth method to check
		switch($permissionLevel) {
			case 'guest':
				# Always allow
				$authenticated = true;
				break;
			case 'user':
				$authenticated = $_SESSION['token'][1] === 0;

				if($authenticated) {
					# Do not continue checking for higher permissions
					# If this isn't triggered, the other permissions will also grant access
					break;
				}

			case 'mod':
				# Allow if the session is a mod one
				$authenticated = $_SESSION['token'][1] === 1;

				if($authenticated) {
					# Do not continue checking for higher permissions
					# If this isn't triggered, the other permissions will also grant access
					break;
				}

			case 'key':
				# Check the api key against the keyfile

				$authentication = false;
				if(!empty($_GET['apikey'])) {
					$apikey = fopen($_SERVER['DOCUMENT_ROOT'].'/config/apikey', 'r');
					$apikey = fread($apikey, filesize($_SERVER['DOCUMENT_ROOT'].'/config/apikey'));

					if($apikey = $_GET['apikey']) {
						$authenticated = true;
					}
				}
		}

		# Check if all required GET variables are non-empty
		foreach($requiredParams as $parameter) {
			if(empty($_GET[$parameter])) {
				$missingParameters = true;
			}
		}

		return $authenticated && !$missingParameters; # Is the user authenticated and has filled out all required parameters?
	}

	$query = 'SELECT id, simplerank as rank, requester, songname, songlink FROM simplerank ORDER BY simplerank ASC LIMIT 5';
	sqlQuery($query, [], []);
	sqlResultsToArray($query, $results);

	switch($_GET['action']) {
		case 'nowPlaying':
			echo json_encode($results[0]);
			break;

		case 'topFive':
			echo json_encode($results);
			break;

		case 'trackExists':
			# Search for the ID in requests
			$query = 'SELECT COUNT(1) FROM requests WHERE id=:id';
			$vars = ['id'];
			$values = [$_GET['id']];

			# Reverse (found = don't skip) and return it as an int
			echo (int)!(boolean)sqlQuery($query, $vars, $values, true)['COUNT(1)'];
			break;

		case 'add':
			if(!actionIsAllowed('user', ['songlink', 'requester'])) { break; }

			if(!preg_match('/https:\/\/www\.youtube\.com.*/', $_GET['songlink']) && !preg_match('/https:\/\/soundcloud\.com.*/', $_GET['songlink'])) {
				http_response_code(400);
				echo 'invalidFormatting';
				die();
			}

			# Validate URL through youtube-dl without downloading anything while also getting the video title
			$escapedURL = escapeshellarg($_GET['songlink']);
			session_write_close(); # Temporarily close the session to allow other page loads during this job
			exec("youtube-dl --no-playlist --get-title $escapedURL", $title, $exit);
			session_start();
			$title = $title[0];
			if($exit !== 0) {
				http_response_code(400);
				echo 'invalidURL';
				die();
			}

			# Get the rank of the last song
			$query = 'SELECT MAX(rank) FROM requests';
			$max = sqlQuery($query, [], [], true)['MAX(rank)'];
			isset($max) ?: $max = 0; # set to 0 if no records exist # set to 0 if no requests exist

			$query = 'INSERT INTO requests (rank, requester, songname, songlink) VALUES (:rank, :requester, :songname, :songlink)';
			$vars = ['rank', 'requester', 'songname', 'songlink'];
			$values = [$max+1, $_GET['requester'], $title, $_GET['songlink']];
			sqlQuery($query, $vars, $values);

			# Fetch the newly added record's simple rank
			$query = 'SELECT simplerank FROM simplerank WHERE id IN ((SELECT id FROM requests WHERE rank=:rank))';
			$vars = ['rank'];
			$values = [$max+1];
			$rank = sqlQuery($query, $vars, $values, true)['simplerank'];

			echo $rank;
			http_response_code(200);
			break;

		case 'delete':
			if(!actionIsAllowed('mod', ['rank'])) { break; }

			$query = 'DELETE FROM requests WHERE id IN ((SELECT id FROM simplerank WHERE simplerank=:rank));';
			$vars = ['rank'];
			$values = [$_GET['rank']];

			$deleteSuccessful = sqlQuery($query, $vars, $values, true)->rowCount();
			if($deleteSuccessful) {
				http_response_code(200);
				echo $results[0]['songname'];
			} else {
				http_response_code(400);
			}
			break;

		case 'songTable':
			if(!actionIsAllowed('guest', ['filter'])) { break; }

			$query =  'SELECT * FROM simplerank';
			$vars = [];
			$values = [];
			if($_GET['filter'] !== 'all') {
				$query = 'SELECT * FROM simplerank where requester=:requester;';
				$vars = ['requester'];
				$values = [$_GET['filter']];
			}

			# Put selected requests into $requests
			sqlQuery($query, $vars, $values);
			sqlResultsToArray($query, $requests);
		?>
			<tr>
				<th>Rank</th>
				<th>Requester</th>
				<th>Name</th>
				<th>Link</th>
				<?php if($_SESSION['token'][1] === 1) { ?><th>Action</td> <?php } ?>
			</tr>
			<?php

			foreach($requests as $request) {
				?>
					<tr>
						<?php if($_SESSION['token'][1] === 1) { ?><td><?php echo $request['simplerank']; ?> (<a href="javascript:ajaxCall('/api/?action=reorder&from=<?php echo $request['simplerank'] ?>&to=<?php echo $request['simplerank'] - 1 ?>');updateSongs();">up</a>/<a href="javascript:ajaxCall('/api/?action=reorder&from=<?php echo $request['simplerank'] ?>&to=<?php echo $request['simplerank'] + 1 ?>');updateSongs();">down</a>)</td> <?php } ?>
						<?php if($_SESSION['token'][1] !== 1) { ?><td><?php echo $request['simplerank']; ?></td> <?php } ?>
						<td><a href="javascript:updateSongs('<?php echo htmlspecialchars($request['requester']); ?>');"><?php echo htmlspecialchars($request['requester']); ?></a></td>
						<td><?php echo $request['songname']; ?></td>
						<td><a href="<?php echo $request['songlink']; ?>"><?php echo htmlspecialchars($request['songlink']); ?></a></td>
						<?php if($_SESSION['token'][1] === 1) { ?><td><a href="javascript:ajaxCall('/api/?action=delete&rank=<?php echo $request['simplerank'] ?>');updateSongs();">Remove</a></td> <?php } ?>
					</tr>
			<?php
			}
			break;

		case 'clear':
			if(!actionIsAllowed('mod')) { break; }

			$query = 'DELETE FROM requests';
			sqlQuery($query);
			break;

		case 'regenUserToken':
			if(!actionIsAllowed('mod')) { break; }

			# Delete old token
			$query = 'DELETE FROM sessions WHERE authtype=0';
			sqlQuery($query);

			# Create and insert new token
			$query = 'INSERT INTO sessions (uuid, authtype) VALUES (:uuid, 0)';
			$vars = ['uuid'];
			$values = [bin2hex(openssl_random_pseudo_bytes(8))]; # Unique ID, see https://stackoverflow.com/questions/1846202/php-how-to-generate-a-random-unique-alphanumeric-string/13733588#13733588
			sqlQuery($query, $vars, $values);

			echo $values[0]; # Return token so that it can be displayed
			break;

		case 'reorder':
			if(!actionIsAllowed('mod', ['to', 'from'])) { break; }

			# Get real rank of from
			$query = 'SELECT rank FROM requests WHERE id IN ((SELECT id FROM simplerank WHERE simplerank=:id))';
			$vars = ['id'];
			$values = [$_GET['from']];
			$from = sqlQuery($query, $vars, $values, true)['rank'];

			# Get real rank of to
			$query = 'SELECT rank FROM requests WHERE id IN ((SELECT id FROM simplerank WHERE simplerank=:id))';
			$vars = ['id'];
			$values = [$_GET['to']];
			$to = sqlQuery($query, $vars, $values, true)['rank'];

			if(!empty($from) && !empty($to)) {
				# Assign -1 to from temporarily
				$query = 'UPDATE requests SET rank=-1 WHERE rank=:id';
				$vars = ['id'];
				$values = [$from];
				sqlQuery($query, $vars, $values);

				$query = 'UPDATE requests SET rank=:from WHERE rank=:to';
				$vars = ['to', 'from'];
				$values = [$to, $from];
				sqlQuery($query, $vars, $values);

				$query = 'UPDATE requests SET rank=:to WHERE rank=-1';
				$vars = ['to'];
				$values = [$to];
				sqlQuery($query, $vars, $values);
			}
			break;
	}
