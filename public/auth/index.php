<?php
	# Copyright © 2020 cofyocepsi@protonmail.com
	#
	# This file is part of https://gitlab.com/cofyocepsi/musicplayer.
	#
	# This project is free software: you can redistribute it and/or modify
	# it under the terms of the GNU Affero General Public License as published by
	# the Free Software Foundation, either version 3 of the License, or
	# (at your option) any later version.
	#
	# This project is distributed in the hope that it will be useful,
	# but WITHOUT ANY WARRANTY; without even the implied warranty of
	# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	# GNU Affero General Public License for more details.
	#
	# You should have received a copy of the GNU Affero General Public License
	# along with this project. If not, see <https://www.gnu.org/licenses/>.

	require($_SERVER['DOCUMENT_ROOT'].'/header.php');

	class linkException extends Exception {
		function errorMessage() {
			return $this->getMessage();
		}
	}

	try {
		# Check if the token is missing or misformatted
		if(empty($_GET['token']) || !preg_match('/^[a-zA-Z0-9]{1,16}$/', $_GET['token'])) {
			throw new linkException('invalidURL');
		}

		# Check if the token exists in the database
		$query = 'SELECT COUNT(1), authtype FROM sessions WHERE uuid=:uuid';
		$vars = ['uuid'];
		$values = [$_GET['token']];
		$result = sqlQuery($query, $vars, $values, true);

		$validToken = $result['COUNT(1)'];
		$authType = $result['authtype'];

		if(!$validToken) {
			throw new linkException('invalidToken');
		}

		# Set the token and redirect to /
		$_SESSION['token'] = [$_GET['token'], $result['authtype']]; # first = token, second = auth type
		header('Location: /');
	}

	catch(linkException $e) {
		echo $e->errorMessage();
	}
