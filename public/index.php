<?php
	# Copyright © 2020 cofyocepsi@protonmail.com
	#
	# This file is part of https://gitlab.com/cofyocepsi/musicplayer.
	#
	# This project is free software: you can redistribute it and/or modify
	# it under the terms of the GNU Affero General Public License as published by
	# the Free Software Foundation, either version 3 of the License, or
	# (at your option) any later version.
	#
	# This project is distributed in the hope that it will be useful,
	# but WITHOUT ANY WARRANTY; without even the implied warranty of
	# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	# GNU Affero General Public License for more details.
	#
	# You should have received a copy of the GNU Affero General Public License
	# along with this project. If not, see <https://www.gnu.org/licenses/>.

	require($_SERVER['DOCUMENT_ROOT'].'/header.php');

	class formException extends Exception {
		function errorMessage() {
			return $this->getMessage();
		}
	}

	try {
		if(empty($_POST)) {
			throw new formException('noSubmission');
		}

		if($_SESSION['token'][1] === -1) {
			throw new formException('insufficientPermissions');
		}

		if(empty($_POST['songlink'])) {
			throw new formException('noURL');
		}

		if(empty($_POST['requester'])) {
			throw new formException('noRequester');
		}

		if(!preg_match('/https:\/\/www\.youtube\.com.*/', $_POST['songlink']) && !preg_match('/https:\/\/soundcloud\.com.*/', $_POST['songlink'])) {
			throw new formException('invalidFormatting');
		}

		# Validate URL through youtube-dl without downloading anything while also getting the video title
		$escapedURL = escapeshellarg($_POST['songlink']);
		session_write_close(); # Temporarily close the session to allow other page loads during this job
		exec("youtube-dl --no-playlist --playlist-end 1 --get-title $escapedURL", $title, $exit);
		session_start();
		$title = $title[0];
		if($exit !== 0) {
			throw new formException('invalidURL');
		}

		# Get the rank of the last song
		$query = 'SELECT MAX(rank) FROM requests';
		$max = sqlQuery($query, [], [], true)['MAX(rank)'];
		isset($max) ?: $max = 0; # set to 0 if no records exist # set to 0 if no requests exist

		$query = 'INSERT INTO requests (rank, requester, songname, songlink) VALUES (:rank, :requester, :songname, :songlink)';
		$vars = ['rank', 'requester', 'songname', 'songlink'];
		$values = [$max+1, $_POST['requester'], $title, $_POST['songlink']];
		sqlQuery($query, $vars, $values);

		# Fetch the newly added record's simple rank
		$query = 'SELECT simplerank FROM simplerank WHERE id IN ((SELECT id FROM requests WHERE rank=:rank))';
		$vars = ['rank'];
		$values = [$max+1];
		$rank = sqlQuery($query, $vars, $values, true)['simplerank'];

		$added = true;
	}

	catch(formException $e) {
		if($e->errorMessage() != 'noSubmission') {
			echo $e->errorMessage();
		}
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Music player</title>
		<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
		<div id="tools">
			<?php
				if($_SESSION['token'][1] !== -1) {
					?>
					<form method="POST" action="">
						<input style="width:300px;" type="text" name="songlink" placeholder="Song link (YT/SoundCloud)">
						<input type="text" name="requester" placeholder="Your name">
						<button type="submit">Submit song</button>
					</form>
					<?php
				}

				switch($_SESSION['token'][1]) {
					case -1:
						echo '<span id="authtype">Authenticated as a guest</span>';
						break;

					case 0:
						echo '<span id="authtype" style="color:#32CD32;margin-bottom:10px;display:block;">Authenticated as a subscriber</span>';
						break;

					case 1:
						echo '<span id="authtype" style="color:#DC143C;">Authenticated as a mod</span>';
						break;
				}

				?>
			<?php
				if($_SESSION['token'][1] === 1) {
					?>
						<ul>
							<li><a href="javascript:ajaxCall('/api/?action=clear');updateSongs();">Clear</a></li>
							<li><a href="javascript:ajaxCall('/api/?action=delete&rank=1');updateSongs();">Skip</a></li>
							<li><a href="javascript:ajaxCall('/api/?action=regenUserToken', function (response) {displayGeneratedToken(response)});">Regenerate user authentication token</a></li>
						</ul>
						<span id="newTokenBox"></span>
					<?php
				}
			?>
		</div>

		Filter:
		<button onclick="updateSongs('all');" id="filter-all">Reset</button>
		<input style="width:200px;" placeholder="Specific user (enter to search)" onkeyup='if(event.key === "Enter") {updateSongs(this.value)}' id="filter-user">
		<?php if(isset($added)) { echo "<br><span style='display: block;margin: 15px 0;'>Added $title at rank $rank</span>"; } ?>
		<table id="songTable"></table>

	<div id="footer">Copyright © 2020 cofyocepsi@protonmail.com. This project is licensed under the AGPL and its source can be found at <a href="https://gitlab.com/cofyocepsi/musicplayer">https://gitlab.com/cofyocepsi/musicplayer</a>.</div>
	</body>
</html>

<script>
	function ajaxCall(url, callback = function() {}) {
		var httpRequest= new XMLHttpRequest();
		httpRequest.onreadystatechange = function() {
			if (httpRequest.readyState == 4 && httpRequest.status == 200) {
				callback(httpRequest.responseText);
			}
		}

		// Send request
		httpRequest.open("GET", url, true);
		httpRequest.send(null);
	}

	var filter = 'all';
	function updateSongs(localFilter = filter) {
		filter = localFilter; // Set default filter
		// Highlight the used filter
		switch(localFilter) {
			case 'all':
				document.getElementById('filter-all').style.borderColor = 'red';
				document.getElementById('filter-user').style.borderColor = '';

				// Clear user filter
				var userfilter = document.getElementById('filter-all').nextSibling.nextSibling.value = '';
				break;
			default:
				document.getElementById('filter-all').style.borderColor = '';
				document.getElementById('filter-user').style.borderColor = '#800000';

				// Set user filter (to ensure that the filter is displayed when set through the function directly)
				var userfilter = document.getElementById('filter-all').nextSibling.nextSibling.value = localFilter;
				break;
		}

		// Update the table with the response
		table = '';
		ajaxCall('/api/?action=songTable&filter='+localFilter, function(table) {
			document.getElementById('songTable').innerHTML = table;
		});

	}

	updateSongs(); // Run manually at page load
	setInterval(function() { updateSongs(); }, 5000); // Update every 5 seconds

	function displayGeneratedToken(response) {
		newTokenBox = document.getElementById('newTokenBox');
		newTokenBox.innerHTML = ''

		// Let the box disappear for .5 seconds to create visual feedback of the next token appearing
		setTimeout(function () {
			newTokenBox.innerHTML = `Click <a href="#" onclick="copyGeneratedToken('${response}')">here</a> to copy the new link`;
		}, 500)
	}

	function copyGeneratedToken(token) {
		str = window.location.hostname+`/auth/?token=${token}`

		// Create new element
		var el = document.createElement('textarea');
		// Set value (string to be copied)
		el.value = str;
		// Set non-editable to avoid focus and move outside of view
		el.setAttribute('readonly', '');
		el.style = {position: 'absolute', left: '-9999px'};
		document.body.appendChild(el);
		// Select text inside element
		el.select();
		// Copy text to clipboard
		document.execCommand('copy');
		// Remove temporary element
		document.body.removeChild(el);
	}
</script>
