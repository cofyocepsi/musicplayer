-- Copyright © 2020 cofyocepsi@protonmail.com
--
-- This file is part of https://gitlab.com/cofyocepsi/musicplayer.
--
-- This project is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This project is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this project. If not, see <https://www.gnu.org/licenses/>.


-- Create sessions table
CREATE TABLE IF NOT EXISTS sessions (
	id INT AUTO_INCREMENT PRIMARY KEY,
	uuid VARCHAR(16) NOT NULL UNIQUE,
	authtype INT(1) NOT NULL DEFAULT 0 -- 0 = user, 1 = mod
);

-- Create requests tabel
CREATE TABLE IF NOT EXISTS requests (
	id INT AUTO_INCREMENT PRIMARY KEY,
	rank INT NOT NULL UNIQUE,
	requester VARCHAR(32) NOT NULL,
	songname VARCHAR(256) NOT NULL,
	songlink VARCHAR(256) NOT NULL,
	voteskips INT NOT NULL DEFAULT 0
);

-- Create simplerank view
CREATE VIEW simplerank AS
SELECT
	id,
	ROW_NUMBER() OVER (ORDER BY rank) AS simplerank,
	requester,
	songname,
	songlink,
	voteskips
FROM requests;
